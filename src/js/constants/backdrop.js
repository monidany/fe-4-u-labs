const BACKDROP = 'backdrop';
const BACKDROP_ACTIVE_CLASS = 'backdrop--active';

export {
  BACKDROP,
  BACKDROP_ACTIVE_CLASS,
};
