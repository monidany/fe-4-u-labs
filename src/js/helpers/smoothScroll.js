const headerEl = document.querySelector('.header__top-bar');

const calcOffset = (el) => {
  const elOffset = el.getBoundingClientRect();
  return (window.scrollY + elOffset.top) - headerEl.offsetHeight * 2;
};

// eslint-disable-next-line
export const smoothScrollToEl = (el) => {
  window.scrollTo({
    top: calcOffset(el),
    behavior: 'smooth',
  });
};
