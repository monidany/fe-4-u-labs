export default {
  setupComponents(components) {
    if (components) {
      components.forEach((component) => {
        if (component.init) {
          component.init();
        } else {
          throw new Error(`No init function for ${component}!`);
        }
      });
    }
  },
};
