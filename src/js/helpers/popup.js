import Backdrop from './backdrop';

import {
  paralyzeScroll,
  unparalyzeScroll,
} from './paralyzeScrollPage';

class Popup {
  constructor(popupEl, { popupVisibleClass, popupTriggers, closeButtonEl }) {
    this.el = popupEl;
    this.triggers = popupTriggers;

    this.popupVisibleClass = popupVisibleClass;
    this.closeButton = closeButtonEl;

    this.backdrop = new Backdrop(this.closePopup.bind(this));

    this.addListeners();
  }

  addListeners() {
    this.triggers.forEach((trigger) => {
      trigger.addEventListener('click', this.openPopup.bind(this));
    });
    this.closeButton.addEventListener('click', this.closePopup.bind(this));
  }

  openPopup(e) {
    e.preventDefault();

    if (!this.isPopupVisible) {
      this.el.classList.add(this.popupVisibleClass);
    }
    this.backdrop.open();
    paralyzeScroll();
  }

  closePopup(e) {
    e.preventDefault();

    if (this.isPopupVisible) {
      this.el.classList.remove(this.popupVisibleClass);
    }
    this.backdrop.close();
    unparalyzeScroll();
  }

  get isPopupVisible() {
    return this.el.classList.contains(this.popupVisibleClass);
  }
}

export default Popup;
