const bodyEl = document.querySelector('body');

const BODY_PARALYZE_CLASS = 'pagescroll--paralyzed';

export const paralyzeScroll = () => {
  bodyEl.classList.add(BODY_PARALYZE_CLASS);
};

export const unparalyzeScroll = () => {
  bodyEl.classList.remove(BODY_PARALYZE_CLASS);
};
