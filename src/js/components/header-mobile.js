import {
  paralyzeScroll,
  unparalyzeScroll,
} from '../helpers/paralyzeScrollPage';
import { smoothScrollToEl } from '../helpers/smoothScroll';

const HEADER_MENU_OPEN_CLASS = 'header__mobile--active';

class HeaderMobile {
  constructor(headerEl) {
    this.el = headerEl;
    this.hamburger = this.el.querySelector('.header__hamburger');

    this.navigationEls = this.el.querySelectorAll('.header__navigation-menu nav a');

    this.addListeners();
  }

  addListeners() {
    this.hamburger.addEventListener('click', this.toggleMenu.bind(this));
    Array.from(this.navigationEls)
      .forEach((navItem) => {
        navItem.addEventListener('click', this.handleNavItemClick.bind(this));
      });
  }

  toggleMenu() {
    // eslint-disable-next-line
    this.isMenuOpen()
      ? this.closeMenu()
      : this.openMenu();
  }

  handleNavItemClick(e) {
    e.preventDefault();

    const targetId = e.currentTarget.getAttribute('href');
    const targetEl = document.querySelector(targetId);

    this.closeMenu();
    smoothScrollToEl(targetEl);
  }

  openMenu() {
    this.el.classList.add(HEADER_MENU_OPEN_CLASS);
    paralyzeScroll();
  }

  closeMenu() {
    this.el.classList.remove(HEADER_MENU_OPEN_CLASS);
    unparalyzeScroll();
  }

  isMenuOpen() {
    return this.el.classList.contains(HEADER_MENU_OPEN_CLASS);
  }
}

export default {
  init() {
    const headerMobile = document.querySelector('.header__mobile');

    if (headerMobile) {
      // eslint-disable-next-line
      new HeaderMobile(headerMobile);
    }
  },
};
