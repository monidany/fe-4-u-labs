import {
  TEACHER_INFO_POPUP_CLOSE_BUTTON_CLASS,
  TEACHER_INFO_POPUP_TRIGGER_CLASS,
  TEACHER_INFO_POPUP_CLASS,
  TEACHER_INFO_POPUP_ACTIVE_CLASS,
} from '../constants/teacher-info-popup';

import Popup from '../helpers/popup';

class TeacherInfoPopup {
  constructor(popupEl) {
    this.el = popupEl;
    this.triggers = document.querySelectorAll(`.${TEACHER_INFO_POPUP_TRIGGER_CLASS}`);

    this.closeButtonEl = this.el.querySelector(`.${TEACHER_INFO_POPUP_CLOSE_BUTTON_CLASS}`);

    this.popup = new Popup(this.el, {
      popupVisibleClass: TEACHER_INFO_POPUP_ACTIVE_CLASS,
      popupTriggers: this.triggers,
      closeButtonEl: this.closeButtonEl,
    });
  }
}

export default {
  init() {
    const teacherInfoPopupEl = document.querySelector(`.${TEACHER_INFO_POPUP_CLASS}`);

    if (teacherInfoPopupEl) {
      // eslint-disable-next-line
      new TeacherInfoPopup(teacherInfoPopupEl);
    }
  },
};
