// Styles
import '../css/app.css';
import '../scss/main.scss';

// Helpers
import initializer from './helpers/initializer';

// Components
import headerMobile from './components/header-mobile';
import addTeacherPopup from './components/add-teacher-popup';
import teacherInfoPopup from './components/teacher-info-popup';

// Initialization of components
const COMPONENTS = [
  headerMobile,
  addTeacherPopup,
  teacherInfoPopup,
];

initializer.setupComponents(COMPONENTS);
